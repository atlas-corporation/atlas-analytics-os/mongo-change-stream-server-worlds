# mongo-change-stream-server-worlds

### A component of the Atlas Analytics platform

## About Atlas Analytics

Atlas Analytics is a platform for obtaining and reporting on analytics in the metaverse which rely on the Decentraland protocol. The platform was made open source as part of a [Decentraland grant](https://decentraland.org/governance/proposal/?id=fe85ab06-618d-4181-960d-fc32d5f0a7e1) and this repository constitues on of the microservices that comprises that platform.

The Atlas Analytics stack has the following components:

* `analytics-ingress` : Express application to receive analytics data from in-world. Contains filters and checks for users to apply if desired.

* `atlas-analytics-app`: The front-end, react application users log into to view reports on their analytics. 

* `analytics-query` : The back-end of the analytics reporting application, providing queries into the database of collected data. Written in python.

* `analytics-express-auth` : A microservice for validating signatures due to functionality unsupported in python. Written in express.

* `analytics-update` : More back-end code, specifically for large queries that require caching periodically. Written in python.

* `analytics-update-cron` : A node-red application governing the periodic triggering of the `analytics-update` caching microservice.

* `mongo-change-stream-server` : An event handler, written in express, to handle keeping a current index of active scenes and last updates.

* **YOU ARE HERE** `mongo-change-stream-server-worlds` : An event handler, written in express, to handle keeping a current index of active scenes and last updates for worlds scenes.

## About Mongo Change Stream Server Worlds

This microservice uses a change stream against a mongo database used to record and persist analytics data. A change stream runs and listens to a specific query from the database and when met performs actions. In this case, it maintains another collection within the same mongo database to contain the current list of scenes and last updates such that this query does not need to be performed upon every user login, the system can rely on this as a cache.

## Deploying this Microservice

This microservice can be deployed using the dockerfile present in the repository. Simply run using `docker build -t mongo-change-stream-server-worlds . && docker run mongo-change-stream-server-worlds`. 

This application can be run anywhere that can access the shared database.
