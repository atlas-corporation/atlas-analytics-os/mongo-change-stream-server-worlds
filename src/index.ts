import dotenv from "dotenv";
import { MongoClient } from "mongodb";

dotenv.config();

const debug = process.env.DEBUG === "true" ? true : false;

const uri = process.env.DB_URI!;

const client = new MongoClient(uri);

const main = async () => {
	try {
		await client.connect();
		console.log("Connected to MongoDB");

		// Process existing documents before watching for new changes
		await processExistingDocuments();

		await watchCollection();
	} catch (err) {
		console.error("Failed to connect to MongoDB", err);
		process.exit(1);
	}
};

const processExistingDocuments = async () => {
	const db = client.db(process.env.DB_NAME_WATCH);
	const collection = db.collection(process.env.DB_COLLECTION_NAME_WATCH!);

	// Define the same condition as your Change Stream for consistency
	const condition = {
		eventName: "load-timer",
		realm:{$exists:true},
		timestamp: { $gte: 1669852800000 }
	};

	// Fetch existing documents that match your condition
	const documents = await collection.find(condition).toArray();

	console.log(`Processing ${documents.length} existing documents`);

	// Process each document using a similar logic to your eventHandler
	for (const doc of documents) {
		
		// Simulate a ChangeStreamMessage structure
		const typedDoc = doc as unknown as FullDocument
		const changeStreamMessage = {
			fullDocument: typedDoc,
			_id: { _data: doc._id.toHexString() }, 
			clusterTime: doc.clusterTime,
			ns: doc.ns, 
			documentKey: doc.documentKey,
			operationType: 'existing' 
		};

		// Use existing eventHandler to process
		try{
			await eventHandler(changeStreamMessage);
		}catch{
			console.log("Failed for doc: " + String(doc._id.toHexString()) )
		}
	}
};

const watchCollection = async () => {
	const db = client.db(process.env.DB_NAME_WATCH!);
	const collection = db.collection(process.env.DB_COLLECTION_NAME_WATCH!);

	const pipeline = [
		{
			$match: {
				operationType: "insert",
				"fullDocument.eventName": "load-timer",
			},
		},
	];

	console.log(
		`Watching Change Stream - ${db.databaseName}:${collection.collectionName}`
	);
	const changeStream = collection.watch(pipeline, {
		fullDocument: "updateLookup",
	});

	changeStream.on("change", (next: ChangeStreamMessage) => {
		//if (debug) console.log("Load time document detected", next);
		eventHandler(next);
	});
};

const eventHandler = async (changeStreamMessage: ChangeStreamMessage) => {
	if (debug) console.dir(changeStreamMessage, { depth: "full" });

	const db = client.db(process.env.DB_NAME_CACHE!);
	const cacheCollection = db.collection(
		process.env.DB_COLLECTION_NAME_CACHE!
	);

	// Extract relevant data from the document
	const sceneName = changeStreamMessage.fullDocument.sceneName;
	const sceneId = changeStreamMessage.fullDocument.sceneId;
	const branch = changeStreamMessage.fullDocument.sceneBranch;
	const parcels = changeStreamMessage.fullDocument.data.sceneInitData.parcels;

	const pollingRate =
		changeStreamMessage.fullDocument.data.sceneInitData.pollingInterval;
	const analyticsVersion =
		changeStreamMessage.fullDocument.data.sceneInitData.analyticsVersion;
	const tags = changeStreamMessage.fullDocument.data.sceneInitData.tags;
	var host = changeStreamMessage.fullDocument.realm
	if(!host){
		host = ""
	}
	var realm = ""
	var world = ""
	const localhosts = ["localhost","0.0.0.0","127.0.0.1","LocalPreview"]
	
	// Bespoke list for worlds type - will require updating overtime or parsing logic
	// Atlas Worlds
	if(host.includes("worlds-server.atlascorp.io")){
		realm = "atlas-worlds"
		try{
			world = host.split('worlds-server.atlascorp.io/content/world/')[1]
		}catch{
			console.log("world not found for host: " + String(host))
		}
		
	}
	// Decentral Games has its own Worlds Server
	else if(host.includes("decentral.games")){
		realm = "decentral-games-worlds"
		try{
			world = host.split('worlds.decentral.games/content/world/')[1]
		}catch{
			console.log("world not found for host: " + String(host))
		}
		
	}
	//In World Builder
	else if(host.includes("dcl-iwb.co")){
		realm = "dcl-in-world-builder"
		try{
			world = host.split('worlds.dcl-iwb.co/world/')[1]
		}catch{
			console.log("world not found for host: " + String(host))
		}
		
	}
	// Local host - players testing locally
	else if (localhosts.some(substring => host.includes(substring))){
		realm = "localhost"
		world = "localhost"
				
	}
	// Foundation worlds
	else if(host.includes(".dcl.eth")){
		realm = "foundation-worlds"
		try{
			world = host.split('.dcl.eth')[0] + ".dcl.eth"
		}catch{
			world = host
		}

	}
	// Filter the tags array to keep only those starting with "atlas:" and extract the Ethereum addresses
	const approvedAccounts = tags
		.filter((tag) => tag.startsWith("atlas:"))
		.map((tag) => tag.replace("atlas:", ""));

	// Construct a unique ID based on the scene name and parcel list
	const uniqueId = `${sceneName}-${realm}-${world}`;
	/*
	if (debug)
		console.log({
			uniqueId,
			sceneName,
			parcels,
			pollingRate,
			analyticsVersion,
			tags,
			approvedAccounts,
		});
		*/
	// Create a filter to find the cache document by the unique ID
	const filter = {
		sceneId: uniqueId,
	};

	const timestamp = Date.now();

	// Prepare the update operation
	console.log(host,realm,world)
	const update = {
		$set: {
			pollingRate: pollingRate,
			analyticsVersion: analyticsVersion,
			approvedAccounts: approvedAccounts,
			updated: timestamp,
		},
		$addToSet: {
			sceneHashes: sceneId,
			branches: branch,
		},
		$setOnInsert: {
			sceneName: sceneName,
			parcels: parcels,
			created: timestamp,
			host: host,
			realm: realm,
			world: world
		},
	};

	//if (debug) console.dir({ filter, update }, { depth: "full" });

	// Upsert option to insert a new document if it doesn't exist
	const options = {
		upsert: true,
	};

	const result = await cacheCollection.updateOne(filter, update, options);

	if (result.upsertedCount) {
		console.log("Created new cache entry:", result.upsertedId);
	} else if (result.modifiedCount) {
		console.log("Updated existing cache entry");
	} else {
		console.log("No cache entry updated");
	}
};

main();
